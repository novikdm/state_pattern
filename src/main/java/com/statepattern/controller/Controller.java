package com.statepattern.controller;

import com.statepattern.model.action.maincontext.MainContext;

public final class Controller {
    private MainContext mainContext = new MainContext();

    public void addUser() {
        mainContext.addUser();
    }

    public void addTask() {
        mainContext.addTask();
    }

    public void addTaskToUser() {
        mainContext.addTaskToUser();
    }

    public String executeAndGet() {
        return mainContext.startAndEnd();
    }
}
