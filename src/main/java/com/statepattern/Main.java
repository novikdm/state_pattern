package com.statepattern;

import com.statepattern.view.Menu;

public class Main {

    public static void main(final String[] args) {
        new Menu().show();
    }
}