package com.statepattern.model.action.taskcontext;

import com.statepattern.model.entity.User;
import com.statepattern.model.action.taskstate.SprintBacklogState;
import com.statepattern.model.action.taskstate.TaskState;

public class TaskContext {
    private TaskState taskState;
    private User user;

    public TaskContext(User newUser) {
        this.user = newUser;
        taskState = new SprintBacklogState();
    }

    public void setTaskState(TaskState state) {
        taskState = state;
    }

    public TaskState getTaskState() {
        return taskState;
    }

    public void setUser(User newUser) {
        user = newUser;
    }

    public User getUser() {
        return user;
    }

    public void toStart() {
        taskState.toStart(this);
    }

    public void toProgress() {
        taskState.toProgress(this);
    }

    public void toReview() {
        taskState.toReview(this);
    }

    public void toTest() {
        taskState.toTest(this);
    }

    public void toBlock() {
        taskState.toBlock(this);
    }

    public void toDone() {
        taskState.toDone(this);
    }
}