package com.statepattern.model.action.maincontext;

import com.statepattern.model.action.mainstate.Begin;
import com.statepattern.model.action.mainstate.MainState;
import com.statepattern.model.entity.Task;
import com.statepattern.model.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class MainContext {
    private MainState state;
    private List<User> users;
    private List<Task> allTasks;
    private final Logger LOGGER = LogManager.getLogger(MainContext.class);

    public MainContext() {
        this.users = new ArrayList<>();
        this.allTasks = new ArrayList<>();
        state = new Begin();
    }

    public void addTask() {
        state.addTask(this);
    }

    public void addUser() {
        state.addUser(this);
    }

    public void addTaskToUser() {
        state.addTaskToUser(this);
    }

    public void endSprint() {
        state.endSprint(this);
    }

    public List<Task> getAllTasks() {
        return allTasks;
    }

    public List<User> getUsers() {
        return users;
    }

    public void startSprint() {
        state.startSprint(this);
    }

    public MainState getState() {
        return state;
    }

    public void setState(MainState state) {
        this.state = state;
    }

    public String startAndEnd() {
        StringBuilder allUsers = new StringBuilder();
        allUsers.append("Users before : \n");
        users.forEach(s -> allUsers.append(s).append("\n"));
        startSprint();
        try {
            Thread.sleep(11000);
        } catch (InterruptedException e) {
            String logMessage = e.getMessage();
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            e.printStackTrace();
        }
        endSprint();
        allUsers.append("Users after : \n");
        getUsers().forEach(s -> allUsers.append(s).append("\n"));
        return allUsers.toString();
    }
}
