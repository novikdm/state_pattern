package com.statepattern.model.action.mainstate;

import com.statepattern.model.action.maincontext.MainContext;
import com.statepattern.model.entity.Task;
import com.statepattern.model.entity.User;

import java.util.ArrayList;
import java.util.List;

public class Sprint implements MainState {
    public void endSprint(MainContext mainContext) {
        List<Task> done = new ArrayList<>();
        List<Task> block = new ArrayList<>();
        for (User user : mainContext.getUsers()) {
            done.addAll(user.getDoneTasks());
            block.addAll(user.getBlockedTasks());
        }
        mainContext.getAllTasks().removeAll(done);
        mainContext.getAllTasks().removeAll(block);
        mainContext.setState(new Begin());
    }
}
