package com.statepattern.model.action.mainstate;

import com.statepattern.model.action.mainstate.helper.LoggerMessages;
import com.statepattern.model.action.maincontext.MainContext;
import com.statepattern.model.action.mainstate.helper.NameGenerator;
import com.statepattern.model.entity.Task;
import com.statepattern.model.entity.User;
import com.statepattern.model.action.taskcontext.TaskContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Begin implements MainState {
    private int taskCounter;
    private int userCounter;
    private final Logger LOGGER = LogManager.getLogger(Begin.class);

    public void addTask(MainContext mainContext) {
        Task task1 = getRandomTask();
        mainContext.getAllTasks().add(task1);
        taskCounter++;
    }

    public void addTaskToUser(MainContext mainContext) {
        boolean taskAvaiable = false;
        User user;
        for (Task task : mainContext.getAllTasks()) {
            if (!task.isUsed()) {
                user = getUserWithLeastTasks(mainContext);
                taskAvaiable = true;
                user.getSprintTasks().add(task);
                task.setUsed(true);
            }
        }
        if (!taskAvaiable) {
            new LoggerMessages().noTaskAvailable();
        }
    }

    public void addUser(MainContext mainContext) {
        mainContext.getUsers().add(new User(userCounter + 1,
                NameGenerator.generateName(), new ArrayList<>(),
                new ArrayList<>(), new ArrayList<>()));
        userCounter++;
    }

    public void startSprint(MainContext mainContext) {
        List<User> users = mainContext.getUsers();
        for (User user : users) {
            new Thread(() -> {
                final int numOfIterationsInSprint = 20;
                final int numOfMethodsInTaskContext = 6;
                TaskContext taskContext = new TaskContext(user);
                List<Task> tasks = taskContext.getUser().getSprintTasks();
                int counter = 0;
                while (!tasks.isEmpty() && counter != numOfIterationsInSprint) {
                    int chosenMethod = new Random()
                            .nextInt(numOfMethodsInTaskContext);
                    try {
                        Thread.sleep(500);
                        counter++;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    invokeCertainMethods(chosenMethod, tasks, taskContext);
                }
            }).start();
        }
        mainContext.setState(new Sprint());
    }

    private void invokeCertainMethods(int chosenMethod, List<Task> tasks,
                                      TaskContext taskContext) {
        int previousTasksSize;
        int currentTaskId;
        if (chosenMethod == 0) {
            currentTaskId = tasks.get(0).getId();
            previousTasksSize = tasks.size();
            taskContext.toStart();
            if (tasks.size() < previousTasksSize) {
                LOGGER.info("Task № " + currentTaskId + " " +
                        "finished");
            }
        } else if (chosenMethod == 1) {
            //this is for testing DoneState case
//            taskContext.toProgress();
//            taskContext.toReview();
//            taskContext.toTest();
            taskContext.toDone();
        } else if (chosenMethod == 2) {
            taskContext.toReview();
        } else if (chosenMethod == 3) {
            taskContext.toBlock();
        } else if (chosenMethod == 4) {
            taskContext.toProgress();
        } else if (chosenMethod == 5) {
            taskContext.toTest();
        }
    }

    private Task getRandomTask() {
        Task task = new Task(taskCounter, "Do this and that");
        return task;
    }

    private User getUserWithLeastTasks(MainContext mainContext) {
        User retUser;
        int leastTasks;
        addUserIfEmpty(mainContext);
        leastTasks = mainContext.getUsers().get(0).getSprintTasks().size();
        retUser = mainContext.getUsers().get(0);
        for (User user : mainContext.getUsers()) {
            if (user.getSprintTasks().isEmpty()) {
                return user;
            } else if (leastTasks > user.getSprintTasks().size()) {
                leastTasks = user.getSprintTasks().size();
                retUser = user;
            }
        }
        return retUser;
    }

    private void addUserIfEmpty(MainContext mainContext) {
        if (mainContext.getUsers().isEmpty()) {
            mainContext.getUsers().add(new User(0,
                    "Ostap Ivanchuk", new ArrayList<>(),
                    new ArrayList<>(), new ArrayList<>()));
        }
    }
}
