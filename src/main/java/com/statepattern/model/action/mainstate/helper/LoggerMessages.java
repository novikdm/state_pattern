package com.statepattern.model.action.mainstate.helper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoggerMessages {
    private final Logger LOGGER = LogManager.getLogger(LoggerMessages.class);

    public void cantDoThisMethod(String methodName) {
        LOGGER.info("Cannot resolve - " + methodName);
    }

    public void noTaskAvailable() {
        LOGGER.info("No task available for this user.");
    }
}
