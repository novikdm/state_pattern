package com.statepattern.model.action.taskstate;

import com.statepattern.model.entity.Task;
import com.statepattern.model.action.taskcontext.TaskContext;

import java.util.List;

public class DoneState implements TaskState {

    @Override
    public void toStart(TaskContext context) {
        List<Task> sprintTasks = context.getUser().getSprintTasks();
        List<Task> doneTasks = context.getUser().getDoneTasks();
        if (!sprintTasks.isEmpty()) {
            doneTasks.add(sprintTasks.get(0));
            sprintTasks.remove(0);
        }
        context.setTaskState(new SprintBacklogState());
    }
}
