package com.statepattern.model.action.taskstate;


import com.statepattern.model.action.taskcontext.TaskContext;

public class ReviewState implements TaskState {

    @Override
    public void toProgress(TaskContext context) {
        context.setTaskState(new InProgressState());
        LOGGER.info("base.Task is in progress.");
    }

    @Override
    public void toTest(TaskContext context) {
        context.setTaskState(new InTestState());
        LOGGER.info("base.Task is testing.");
    }

    @Override
    public void toBlock(TaskContext context) {
        context.setTaskState(new BlockState());
        LOGGER.info("base.Task is blocked.");
    }
}