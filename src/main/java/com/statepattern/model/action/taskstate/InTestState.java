package com.statepattern.model.action.taskstate;


import com.statepattern.model.action.taskcontext.TaskContext;

public class InTestState implements TaskState {

    @Override
    public void toProgress(TaskContext context) {
        context.setTaskState(new InProgressState());
        LOGGER.info("base.Task is in progress.");
    }

    @Override
    public void toBlock(TaskContext context) {
        context.setTaskState(new BlockState());
        LOGGER.info("base.Task is blocked.");
    }

    @Override
    public void toDone(TaskContext context) {
        context.setTaskState(new DoneState());
        LOGGER.info("base.Task is done.");
    }
}
