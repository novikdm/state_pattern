package com.statepattern.model.action.taskstate;


import com.statepattern.model.action.taskcontext.TaskContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface TaskState {
    Logger LOGGER = LogManager.getLogger(TaskState.class);

    default void toStart(TaskContext context) {
        LOGGER.info("toStart -> is not allowed");
    }

    default void toProgress(TaskContext context) {
        LOGGER.info("toProgress -> is not allowed");
    }

    default void toReview(TaskContext context) {
        LOGGER.info("toReview -> is not allowed");
    }

    default void toTest(TaskContext context) {
        LOGGER.info("toTest -> is not allowed");
    }

    default void toBlock(TaskContext context) {
        LOGGER.info("toBlock -> is not allowed");
    }

    default void toDone(TaskContext context) {
        LOGGER.info("toDone -> is not allowed");
    }
}