package com.statepattern.model.entity;

import java.util.List;
import java.util.Objects;

public class User {
    private int id;
    private String name;
    private List<Task> sprintTasks;
    private List<Task> doneTasks;
    private List<Task> blockedTasks;

    public User() {
    }

    public User(int id, String name, List<Task> sprintTasks,
                List<Task> doneTasks, List<Task> blockedTasks) {
        this.id = id;
        this.name = name;
        this.sprintTasks = sprintTasks;
        this.doneTasks = doneTasks;
        this.blockedTasks = blockedTasks;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Task> getSprintTasks() {
        return sprintTasks;
    }

    public void setSprintTasks(List<Task> sprintTasks) {
        this.sprintTasks = sprintTasks;
    }

    public List<Task> getDoneTasks() {
        return doneTasks;
    }

    public void setDoneTasks(List<Task> doneTasks) {
        this.doneTasks = doneTasks;
    }

    public List<Task> getBlockedTasks() {
        return blockedTasks;
    }

    public void setBlockedTasks(List<Task> blockedTasks) {
        this.blockedTasks = blockedTasks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                Objects.equals(name, user.name) &&
                Objects.equals(sprintTasks, user.sprintTasks) &&
                Objects.equals(doneTasks, user.doneTasks) &&
                Objects.equals(blockedTasks, user.blockedTasks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, sprintTasks, doneTasks, blockedTasks);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sprintTasks=" + sprintTasks +
                ", doneTasks=" + doneTasks +
                ", blockedTasks=" + blockedTasks +
                '}';
    }
}
